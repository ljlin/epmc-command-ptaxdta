/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package epmc.udbm;

public class Federation {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected Federation(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(Federation obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        udbm_intJNI.delete_Federation(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public Federation(int dim) {
    this(udbm_intJNI.new_Federation__SWIG_0(dim), true);
  }

  public Federation(int dim, AtomConstraint t) {
    this(udbm_intJNI.new_Federation__SWIG_1(dim, AtomConstraint.getCPtr(t), t), true);
  }

  public Federation(Federation arg0) {
    this(udbm_intJNI.new_Federation__SWIG_2(Federation.getCPtr(arg0), arg0), true);
  }

  public String toStr(VarNamesAccessor arg0) {
    return udbm_intJNI.Federation_toStr(swigCPtr, this, VarNamesAccessor.getCPtr(arg0), arg0);
  }

  public Federation orOp(Federation arg0) {
    return new Federation(udbm_intJNI.Federation_orOp(swigCPtr, this, Federation.getCPtr(arg0), arg0), true);
  }

  public Federation andOp(Federation arg0) {
    return new Federation(udbm_intJNI.Federation_andOp__SWIG_0(swigCPtr, this, Federation.getCPtr(arg0), arg0), true);
  }

  public Federation andOp(AtomConstraint arg0) {
    return new Federation(udbm_intJNI.Federation_andOp__SWIG_1(swigCPtr, this, AtomConstraint.getCPtr(arg0), arg0), true);
  }

  public Federation addOp(Federation arg0) {
    return new Federation(udbm_intJNI.Federation_addOp(swigCPtr, this, Federation.getCPtr(arg0), arg0), true);
  }

  public Federation minusOp(Federation arg0) {
    return new Federation(udbm_intJNI.Federation_minusOp(swigCPtr, this, Federation.getCPtr(arg0), arg0), true);
  }

  public void up() {
    udbm_intJNI.Federation_up(swigCPtr, this);
  }

  public void down() {
    udbm_intJNI.Federation_down(swigCPtr, this);
  }

  public void mergeReduce(int arg0, int arg1) {
    udbm_intJNI.Federation_mergeReduce(swigCPtr, this, arg0, arg1);
  }

  public void freeClock(int i) {
    udbm_intJNI.Federation_freeClock(swigCPtr, this, i);
  }

  public boolean lt(Federation arg0) {
    return udbm_intJNI.Federation_lt(swigCPtr, this, Federation.getCPtr(arg0), arg0);
  }

  public boolean gt(Federation arg0) {
    return udbm_intJNI.Federation_gt(swigCPtr, this, Federation.getCPtr(arg0), arg0);
  }

  public boolean le(Federation arg0) {
    return udbm_intJNI.Federation_le(swigCPtr, this, Federation.getCPtr(arg0), arg0);
  }

  public boolean ge(Federation arg0) {
    return udbm_intJNI.Federation_ge(swigCPtr, this, Federation.getCPtr(arg0), arg0);
  }

  public boolean eq(Federation arg0) {
    return udbm_intJNI.Federation_eq(swigCPtr, this, Federation.getCPtr(arg0), arg0);
  }

  public void setZero() {
    udbm_intJNI.Federation_setZero(swigCPtr, this);
  }

  public void predt(Federation arg0) {
    udbm_intJNI.Federation_predt(swigCPtr, this, Federation.getCPtr(arg0), arg0);
  }

  public void intern() {
    udbm_intJNI.Federation_intern(swigCPtr, this);
  }

  public void setInit() {
    udbm_intJNI.Federation_setInit(swigCPtr, this);
  }

  public void convexHull() {
    udbm_intJNI.Federation_convexHull(swigCPtr, this);
  }

  public boolean containsIntValuation(IntClockValuation arg0) {
    return udbm_intJNI.Federation_containsIntValuation(swigCPtr, this, IntClockValuation.getCPtr(arg0), arg0);
  }

  public boolean containsDoubleValuation(DoubleClockValuation arg0) {
    return udbm_intJNI.Federation_containsDoubleValuation(swigCPtr, this, DoubleClockValuation.getCPtr(arg0), arg0);
  }

  public void myExtrapolateMaxBounds(IntVector arg0) {
    udbm_intJNI.Federation_myExtrapolateMaxBounds(swigCPtr, this, IntVector.getCPtr(arg0), arg0);
  }

  public boolean hasZero() {
    return udbm_intJNI.Federation_hasZero(swigCPtr, this);
  }

  public void updateValue(int x, int v) {
    udbm_intJNI.Federation_updateValue__SWIG_0(swigCPtr, this, x, v);
  }

  public int size() {
    return udbm_intJNI.Federation_size(swigCPtr, this);
  }

  public int hash() {
    return udbm_intJNI.Federation_hash(swigCPtr, this);
  }

  public boolean isEmpty() {
    return udbm_intJNI.Federation_isEmpty(swigCPtr, this);
  }

  public void updateValue(long x, int v) {
    udbm_intJNI.Federation_updateValue__SWIG_1(swigCPtr, this, x, v);
  }

  public void updateClock(long x, long y) {
    udbm_intJNI.Federation_updateClock(swigCPtr, this, x, y);
  }

  public void updateIncrement(long x, int v) {
    udbm_intJNI.Federation_updateIncrement(swigCPtr, this, x, v);
  }

  public void update(long x, long y, int v) {
    udbm_intJNI.Federation_update(swigCPtr, this, x, y, v);
  }

}
